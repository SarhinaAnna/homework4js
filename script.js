"use strict";
let number1 = prompt("Enter first number");
let number2 = prompt("Enter second number");

let mathOperation = prompt("Enter Math operation + or - or * or /");

function operation(number1, mathOperation, number2) {
  if (mathOperation === "+") {
    return number1 + number2;
  }
  if (mathOperation === "-") {
    return number1 - number2;
  }
  if (mathOperation === "*") {
    return number1 * number2;
  }
  if (mathOperation === "/") {
    return number1 / number2;
  }
}

console.log(operation(+number1, mathOperation, +number2));
